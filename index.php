<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="estil.css">
    <link rel="stylesheet" href="red.css">
</head>
<body>

    <div class="header-inner">
        <!-- HEADER CONTENT : begin -->
        <div class="header-content">
            <div class="c-container">
                <div class="header-content-inner">

                    <!-- HEADER BRANDING : begin -->
                    <!-- Logo dimensions can be changed in library/css/custom.css
                    You can remove "m-large-logo" class from following element to use standard (smaller) version of logo -->
                    <div class="header-branding m-large-logo">
                        <a href="index.html"><span>
                            <img src="images/Logotipo.png" data-hires="images/header-logo.2x.png" alt="TownPress - Municipality HTML Template">
                        </span></a>
                    </div>
                    <!-- HEADER BRANDING : end -->

                </div>
            </div>
        </div>
        <!-- HEADER CONTENT : end -->
    </div>
 
        
    <div class="header-bg">
        <!-- HEADER IMAGE : begin -->
        <!-- To add more images just copy and edit elements with "image-layer" class (see home-2.html template for example)
        Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
        <div class="header-image" data-autoplay="10">
            <div class="image-layer m-active" style="background-image: url( 'images/cap1.webp' );"></div>
            <div class="image-layer m-next" style="background-image: url( 'images/cap2.webp' );"></div>
        </div>
        <!-- HEADER IMAGE : begin -->
    </div>

    <div id="core">
        <div class="c-container">
            <div class="row">

            </div>
        </div>
    </div>
    <script src="jquery-1.9.1.min.js" type="text/javascript"></script>
    <span id="media-query-breakpoint" style="display: none;"></span>
    <script src="http://demos.lsvr.sk/townpress.html/demo/library/js/third-party.js" type="text/javascript"></script>
    <script src="http://demos.lsvr.sk/townpress.html/demo/library/js/scripts.js" type="text/javascript"></script>
</body>
</html>