/* -----------------------------------------------------------------------------

	TABLE OF CONTENTS

	1.) General
	2.) Components
	3.) Header
	4.) Widgets
	5.) Sidebar

----------------------------------------------------------------------------- */

(function($){ "use strict";
$(document).ready(function(){
/* -----------------------------------------------------------------------------

	1.) GENERAL

----------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------
		CHECK FOR TOUCH DISPLAY
	------------------------------------------------------------------------- */

	$( 'body' ).one( 'touchstart', function(){
		$(this).addClass( 'm-touch' );
	});

	/* -------------------------------------------------------------------------
		INIT PAGE
	------------------------------------------------------------------------- */

	if ( ! $.fn.lsvrInitPage ) {
		$.fn.lsvrInitPage = function( element ){

			var $element = $( element );

			// FLUID MEDIA
			if ( $.fn.lsvrFluidEmbedMedia ){
				$element.lsvrFluidEmbedMedia();
			}

			// LIGHTBOXES
			if ( $.fn.lsvrInitLightboxes ) {
				$element.lsvrInitLightboxes();
			}

			// LOAD HIRES IMAGES FOR HiDPI SCREENS
			if ( $.fn.lsvrLoadHiresImages ) {
				$element.lsvrLoadHiresImages();
			}

			// AJAX FORMS
			if ( $.fn.lsvrAjaxForm ) {
				$element.find( 'form.m-ajax-form' ).each(function(){
					$(this).lsvrAjaxForm();
				});
			}

		};
	}
    $.fn.lsvrInitPage( 'body' );
    
    /* -------------------------------------------------------------------------
		MEDIA QUERY BREAKPOINT
	------------------------------------------------------------------------- */

	var mediaQueryBreakpoint;
	if ( $.fn.lsvrGetMediaQueryBreakpoint ) {
		mediaQueryBreakpoint = $.fn.lsvrGetMediaQueryBreakpoint();
		$( document ).on( 'screenTransition', function(){
			mediaQueryBreakpoint = $.fn.lsvrGetMediaQueryBreakpoint();
		});
	}
	else {
		mediaQueryBreakpoint = $(window).width();
    }
    
    /* -------------------------------------------------------------------------
		IMAGE SLIDESHOW
	------------------------------------------------------------------------- */

	$( '.header-image' ).each(function(){
		if ( mediaQueryBreakpoint > 991 && $.timer && $(this).data( 'autoplay' ) && $(this).find( '.image-layer' ).length > 1 ) {

			var $this = $(this),
				layers = $this.find( '.image-layer' ),
				interval = parseInt( $this.data( 'autoplay' ) ),
				timer;

			layers.filter( ':eq(0)' ).addClass( 'm-active' );
			layers.filter( ':eq(1)' ).addClass( 'm-next' );

			interval = interval < 1 ? 0 : interval * 1000;

			if ( interval > 0 ) {

				// START SLIDESHOW
				timer = $.timer( interval, function(){
					layers.filter( '.m-active' ).fadeOut( 900, function(){
						$(this).removeClass( 'm-active' ).css( 'display', '' );
						layers.filter( '.m-next' ).addClass( 'm-active' ).removeClass( 'm-next' );
						if ( layers.filter( '.m-active' ).is( ':last-child' ) ) {
							layers.filter( ':eq(0)' ).addClass( 'm-next' );
						}
						else {
							layers.filter( '.m-active' ).next().addClass( 'm-next' );
						}
					});
				});

				// PAUSE WHEN MAP IS OPENED
				$( document ).on( 'headerMapOpened', function(){
					timer.pause();
				});

				// RESUME WHEN MAP IS CLOSED
				$( document ).on( 'headerMapClosed', function(){
					timer.resume();
				});

			}

		}
    });

    /* -------------------------------------------------------------------------
		HEADER TOGGLE
	------------------------------------------------------------------------- */

	$( '.header-toggle' ).each(function(){

		var $this = $(this);
		$this.click( function(){

			// HIDE
			if ( $( '.header-tools' ).is( ':visible') ) {
				$this.removeClass( 'm-active' );
				$( '.header-menu, .header-tools' ).slideUp(300);
			}
			// SHOW
			else {
				$this.addClass( 'm-active' );
				$( '.header-menu, .header-tools' ).slideDown(300);
			}

		});

		// RESET ON SCREEN TRANSITION
		$( document ).on( 'screenTransition', function(){
			$this.removeClass( 'm-active' );
			$( '.header-menu, .header-tools' ).removeAttr( 'style' );
		});

    });

    
});
})(jQuery);